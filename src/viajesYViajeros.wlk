class Viajero {
	var viajes = []
	
	method paisesDeResidencia(anio)

	method enQuePaisesEstuvo_maaaaaal(anio) {
		var paises = viajes
			.filter({viaje => true})   // TODO poner la condicion
			.map({viaje => viaje.pais()})
			.asSet()
//		if (self.esEstablecido()) {  esto es lo que está mal,
//								   las preguntas por el "tipo" se resuelven
//								   usando herencia o delegacion
//			paises.add(...)
//		}
		return paises
	}
	
	method enQuePaisesEstuvo_primera_version(anio) {
		var paises = viajes
			.filter({viaje => true})   // TODO poner la condicion
			.map({viaje => viaje.pais()})
			.asSet()
		paises.addAll(self.paisesDeResidencia(anio))
		return paises
	}
	
	method paisesDondeViajo(anio) {
		return viajes
			.filter({viaje => viaje.anio() == anio})   
			.map({viaje => viaje.pais()})
			.asSet() // estoy diciendo "quiero que esto sea un Set,
						// independientemente de si viajes es Set o List
	}
	
	method agregarViaje(viaje) {
//		viajes + #{viaje}  esto no esta bien
		viajes.add(viaje)
	}
	
	method viajoA(unPais, unAnio) {
		viajes.add(new Viaje(anio = unAnio, pais = unPais))
	}

	method enQuePaisesEstuvo_template_method(anio) {
		return
			self.paisesDondeViajo(anio) +
			self.paisesDeResidencia(anio)
	}
	
	method enQuePaisesEstuvo_redefinicion(anio) {
		return self.paisesDondeViajo(anio)
	}	
}

class Establecido_Redefinicion inherits Viajero {
	var property paisDondeVive
//	override method paisesDeResidencia(anio) { return #{}} // TODO implementar
	override method enQuePaisesEstuvo_redefinicion(anio) {
		return super(anio) + #{ paisDondeVive }
	}		
}

class Establecido inherits Viajero {
	override method paisesDeResidencia(anio) { return #{}} // TODO implementar	
}


class Migrante inherits Viajero {
	var property nacioEn
	var dondeMigro
	var cuandoMigro
//	var property migroA   no funciona, el setter tiene mas de un parametro
	
	method migroA(pais, anio) {
		dondeMigro = pais
		cuandoMigro = anio
	}
	override method paisesDeResidencia(anio) { return #{}} // TODO implementar	
}

class Viaje {
	var anio
	var pais
	
	method anio() { return anio }
	method pais() { return pais }
}






















